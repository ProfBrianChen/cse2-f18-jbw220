///////////////
////
///HW #4 Jamie Wisnia 9-24-18

import java.util.Scanner;
import java.util.Random;
public class CrapsIf{
  public static void main (String [] args) {
    System.out.println("Would you like randomly cast dice or state the two dice that you want to evaluate?");
    // Declare an instance of the scanner object and call the Scanner constructor.  
    Scanner myScanner = new Scanner( System.in );
    //Prompt the user for the option of rolling the dice randomly, or imputing the numbers in themselves 
    System.out.println("If you would like to randomly cast dice, type 1: ");
    System.out.println("If you would like to choose your own dice, type 2: ");
    //Accept user input by using the statement
    int choice = myScanner.nextInt();
    if (choice > 2 || choice < 1){
      System.out.println("You did not input a valid number.");
    }
    
    else if (choice == 1) {
      Random rand = new Random();
      int dice1 = rand.nextInt(6) + 1;
      int dice2 = rand.nextInt(6) + 1;
      System.out.println("Dice one is:" + dice1);
      System.out.println("Dice two is:" + dice2);
      
      if ((dice1 == 1 && dice2 == 2) || (dice2 == 1 && dice1 == 2)){
      System.out.println("Ace Deuce!");
      }
      else if ((dice1 + dice2) == 5){
      System.out.println("Fever Five!");
      }
      else if (((dice1 + dice2) == 6) && (dice1 != 3)){
      System.out.println("Easy Six!");
      }
      else if ((dice1 + dice2 == 6) && (dice1 == 3)){
      System.out.println("Hard Six!");
      }
      else if ((dice1 + dice2) == 2){
          System.out.println("Snake Eyes!");
      }
      else if (((dice1 + dice2) == 4 ) && (dice1 !=2)){
        System.out.println("Easy Four!");
      }
      else if(((dice1 + dice2) == 4 ) && (dice1 == 2)){
        System.out.println("Hard Four!");
      }
      else if ((dice1 + dice2) == 7){
        System.out.println("Seven Out!");
      }
      else if (((dice1 + dice2) == 8) && (dice1 != 4)){
        System.out.println("Easy Eight!");
      }
      else if (((dice1 + dice2) == 8) && (dice1 == 4)){
        System.out.println("Hard Eight!");
      }
      else if ((dice1 + dice2) == 9){
        System.out.println("Nine!");
      }
      else if (((dice1 + dice2) == 10) && (dice1 != 5)){
        System.out.println("Easy Ten!");
      }
      else if (((dice1 + dice2) == 10) && (dice1 == 5)){
        System.out.println("Hard Ten!");
      }
      else if ((dice1 + dice2) == 11){
        System.out.println("Yo-Leven!");
      }
      else if ((dice1 + dice2) == 12){
        System.out.println("BoxCars!");
      }
    }
    
    else if (choice == 2) {
      System.out.println("What would you like the value of Dice 1 to be? ");
      int dice1 = myScanner.nextInt();
      if (dice1 > 6 || dice1 < 1){
        System.out.println("You did not input a valid number.");
        return;
      }
      
      System.out.println("What would you like the value of Dice 2 to be? ");
      int dice2 = myScanner.nextInt();
      
      if (dice2 > 6 || dice2 < 1){
        System.out.println("You did not input a valid number.");
        return;
      }
      
      if ((dice1 == 1 && dice2 == 2) || (dice2 == 1 && dice1 == 2)){
        System.out.println("Ace Deuce!");
      }
      else if ((dice1 + dice2) == 5){
        System.out.println("Fever Five!");
      }
      else if (((dice1 + dice2) == 6) && (dice1 != 3)){
        System.out.println("Easy Six!");
      }
      else if ((dice1 + dice2 == 6) && (dice1 == 3)){
        System.out.println("Hard Six!");
      }
      else if ((dice1 + dice2) == 2){
        System.out.println("Snake Eyes!");
      }
      else if (((dice1 + dice2) == 4 ) && (dice1 !=2)){
        System.out.println("Easy Four!");
      }
      else if(((dice1 + dice2) == 4 ) && (dice1 == 2)){
        System.out.println("Hard Four!");
      }
      else if ((dice1 + dice2) == 7){
        System.out.println("Seven Out!");
      }
      else if (((dice1 + dice2) == 8) && (dice1 != 4)){
        System.out.println("Easy Eight!");
      }
      else if (((dice1 + dice2) == 8) && (dice1 == 4)){
        System.out.println("Hard Eight!");
      }
      else if ((dice1 + dice2) == 9){
        System.out.println("Nine!");
      }
      else if (((dice1 + dice2) == 10) && (dice1 != 5)){
        System.out.println("Easy Ten!");
      }
      else if (((dice1 + dice2) == 10) && (dice1 == 5)){
        System.out.println("Hard Ten!");
      }
      else if ((dice1 + dice2) == 11){
        System.out.println("Yo-Leven!");
      }
      else if ((dice1 + dice2) == 12){
        System.out.println("BoxCars!");
      }
    }
    
    
    
  }
}


