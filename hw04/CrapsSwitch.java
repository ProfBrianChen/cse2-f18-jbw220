///////////////
////
///HW #4 Jamie Wisnia 9-24-18
// Declare scanner & Random and Class
import java.util.Scanner;
import java.util.Random;
public class CrapsSwitch{
  // Main method like every single code
  public static void main (String [] args) {
    // declare variables 
    int dice1 = 0;
    int dice2 = 0;
    
    System.out.println("Would you like randomly cast dice or state the two dice that you want to evaluate?");
    // Declare an instance of the scanner object and call the Scanner constructor.  
    Scanner myScanner = new Scanner( System.in );
    //Prompt the user for the option of rolling the dice randomly, or imputing the numbers in themselves 
    System.out.println("If you would like to randomly cast dice, type 1: ");
    System.out.println("If you would like to choose your own dice, type 2: ");
    //Accept user input by using the statement
    int choice = myScanner.nextInt();
    // In case the user inputs the wrong number 
    if (choice > 2 || choice < 1){
      System.out.println("You did not input a valid number.");
    }
    // If the user wants to use the randomly selected dice 
    else if (choice == 1) {
      Random rand = new Random();
      dice1 = rand.nextInt(6) + 1;
      dice2 = rand.nextInt(6) + 1;
      // Tells the user what the two dice values are
      System.out.println("Dice one is:" + dice1);
      System.out.println("Dice two is:" + dice2);
    }
    // In case the user wants to input their own values into the code
      else if (choice == 2) {
      System.out.println("What would you like the value of Dice 1 to be? ");
      dice1 = myScanner.nextInt();
      if (dice1 > 6 || dice1 < 1){
        System.out.println("You did not input a valid number.");
        return;
      }
      
      System.out.println("What would you like the value of Dice 2 to be? ");
      dice2 = myScanner.nextInt();
      //This is in case the user did not enter a valid number
      if (dice2 > 6 || dice2 < 1){
        System.out.println("You did not input a valid number.");
        return;
      }
        
      }
    //This begins our switch statments. This is for case one: AKA dice 1 =1 and what Dice2 =
     switch(dice1){
        case 1: 
          switch(dice2){
            case 1:
               System.out.println("Snake Eyes!");
              break;
            case 2:
               System.out.println("Ace Deuce!"); 
               break;
            case 3:
                System.out.println("Easy Four!");
               break;
            case 4:
                System.out.println("Fever Five!");
               break;
            case 5:
                System.out.println("Easy Six!");
               break;
            case 6:
                System.out.println("Seven Out!");
               break;
          }
           break;
        case 2:
          switch(dice2){
            case 1:
              System.out.println("Ace Deuce!");
               break;
            case 2: 
              System.out.println("Hard Four!");
               break;
            case 3: 
              System.out.println("Fever Five!");
               break;
            case 4: 
              System.out.println("Easy Six!");
               break;
            case 5:
              System.out.println("Seven Out!");
               break;
            case 6: 
              System.out.println("Easy Eight!");
               break;
          }
         break;
       case 3:
         switch(dice2){
           case 1:
             System.out.println("Easy Four!");
             break;
           case 2: 
             System.out.println("Fever Five!");
             break;
           case 3:
             System.out.println("Hard Six!");
             break;
           case 4:
             System.out.println("Seven Out!");
             break;
           case 5:
             System.out.println("Easy Eight!");
             break;
           case 6:
             System.out.println("Nine!");
             break;
         }
           break;
       case 4:
         switch(dice2){
           case 1:
             System.out.println("Fever Five!");
             break;
           case 2:
             System.out.println("Easy Six!");
             break;
           case 3:
             System.out.println("Seven Out!");
             break;
           case 4:
             System.out.println("Hard Eight!");
             break;
           case 5:
             System.out.println("Nine!");
             break;
           case 6: 
             System.out.println("Easy Ten!");
             break;
         }
           break;
       case 5:
         switch(dice2){
           case 1:
             System.out.println("Easy Six!");
             break;
           case 2:
             System.out.println("Seven Out!");
             break;
           case 3:
             System.out.println("Easy Eight!");
             break;
           case 4:
             System.out.println("Nine!");
             break;
           case 5: 
             System.out.println("Hard Ten!");
             break;
           case 6:
             System.out.println("Yo-leven!");
             break;
         }
           break;
       case 6:
         switch(dice2){
           case 1: 
             System.out.println("Seven Out!");
             break;
           case 2: 
             System.out.println("Easy Eight!");
             break;
           case 3:
             System.out.println("Nine!");
             break;
           case 4:
             System.out.println("Easy Ten!");
             break;
           case 5:
             System.out.println("Yo-leven!");
             break;
           case 6:
             System.out.println("Boxcars!");
             break;
         }
           break;
         }
         }
         }



