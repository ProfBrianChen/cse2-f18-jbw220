//////////////
//// CSE 02 Welcome Class by Jamie Wisnia Section 112
///
public class WelcomeClass{
  
  public static void main(String args[]) {
    ///prints ----------- to terminal window
    System.out.println("  -----------  ");
      ///prints | WELCOME | to terminal window
    System.out.println("  | WELCOME |  ");
      ///prints ----------- to terminal window
      System.out.println("  -----------  ");
    ///prints ^ ^ ^ ^ ^ ^ to terminal window 
    System.out.println("  ^  ^  ^  ^  ^  ^  ");
    ///prints / \/ \/ \/ \/ \/ \ to terminal window
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\ ");
    ///prints <-J--B--W--2--2--0->
    System.out.println("<-J--B--W--2--2--0->");
    ///prints \ /\ /\ /\ /\ /\ / to terminal window 
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ / ");
    ///prints v v v v v v to terminal window
    System.out.println("  v  v  v  v  v  v ");
  }
  
}