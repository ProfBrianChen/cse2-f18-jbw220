//Jamie Wisnia CSE 002 Lab 08: 11/9/18
/*Write a program that performs the following tasks:

1)  Create 2 array of integers with each having a size of 100.
2)    Fill in one of the arrays with randomized integers in the range of 0 to 99, using math.random().
3)  Use the second array to hold the number of occurrences of each number in the first array.
4)  See sample run and output below. Note for the sample run I am using less than 100 numbers

*/
import java.util.Arrays;
public class Array {
    public static void main(String[] args) {
        int j = 0;
        int k = 0;
        int w = -1;
        int[] x = new int[100];
        int[] store = new int[100];
        for (int y = 0; y < x.length; y++) {
            x[y] = (int) (Math.random() * (100));
            //System.out.println(x[y]);
        }
        System.out.println(Arrays.toString(x));
        for (k = 0; k < store.length; k++) {
            int count = 0;
            w++;

            for (j = 0; j < x.length; j++) {
                if (k == x[j]) {
                    count++;
                }
            }
            store[k] = count;
            System.out.print("The number of occurences of random integer ");
            System.out.print(w + " is: ");
            System.out.println(store[k]);
        }
    }
}
