//////////////
////Jamie Wisnia Lab 04 Card Generator 9-21-18
///
/// This code is designed to pick a card at random, and tell you which card it picked
///
import java.util.Random;
public class CardGenerator{
  //main method
  public static void main(String[]args){
    Random rand = new Random();
    int n = rand.nextInt(52) + 1;
    //System.out.println("your number is " +n);
    //This allows us to have the remainder be used for all four suits. 
    int randomCard = n % 13;
    //System.out.println(randomCard);
    //This if statment is for diamonds. If the number is between 1 and 13, then it will be diamonds. 
    if (n <= 13){
      String suit = "diamonds.";
      switch (randomCard){
        case 0: 
          System.out.println("You have an Ace of " + suit);
          break;
        case 11:
          System.out.println("You have a Jack of " + suit);
          break;
        case 12:
          System.out.println("You have a Queen of " + suit);
          break;
        case 13:
          System.out.println("You have a King of " + suit);
          break;
        default: System.out.println("You have a " + randomCard + " of " + suit);
      }
    }
    
    else if (n <= 26){
      String suit = "clubs.";
      switch (randomCard){
        case 0: 
          System.out.println("You have an Ace of " + suit);
          break;
        case 11:
          System.out.println("You have a Jack of " + suit);
          break;
        case 12:
          System.out.println("You have a Queen of " + suit);
          break;
        case 13:
          System.out.println("You have a King of " + suit);
          break;
        default: System.out.println("You have a " + randomCard + " of " + suit);
      }
    }
    else if (n <= 39){
      String suit = "spades.";
      switch (randomCard){
        case 0: 
          System.out.println("You have an Ace of " + suit);
          break;
        case 11:
          System.out.println("You have a Jack of " + suit);
          break;
        case 12:
          System.out.println("You have a Queen of " + suit);
          break;
        case 13:
          System.out.println("You have a King of " + suit);
          break;
        default: System.out.println("You have a " + randomCard + " of " + suit);
      }
    }
    else if (n <= 52){
      String suit = "hearts.";
      switch (randomCard){
        case 0: 
          System.out.println("You have an Ace of " + suit);
          break;
        case 11:
          System.out.println("You have a Jack of " + suit);
          break;
        case 12:
          System.out.println("You have a Queen of " + suit);
          break;
        case 13:
          System.out.println("You have a King of " + suit);
          break;
        default: System.out.println("You have a " + randomCard + " of " + suit);
      }
    }
    
    
    
    
    
    
    // 52 is the maximum and 1 is our minimum 
   /*if (n <= 13){
     if (n <= 10){
     System.out.print("you have the " +n);
    System.out.println(" of diamonds"); }
     if (n == 11)
      System.out.println("you have the jack of diamonds");    
    if (n == 12)
       System.out.println("you have the queen of diamonds");    
    if (n ==13)
       System.out.println("you have the king of diamonds");    
   }
    if (n <= 10)
     System.out.print("you have the " +n);
    System.out.println(" of diamonds");
    if (n == 11)
      System.out.println("you have the jack of diamonds");    
    if (n == 12)
       System.out.println("you have the queen of diamonds");    
    if (n ==13)
       System.out.println("you have the king of diamonds");    
    if ( 13 <n && n <= 26 )
      if (n <= 23)
        int c = ((int n)-13);
    System.out.print("you have the " +c);
    System.out.println(" of clubs");
      System.out.println("your card is clubs");
    if (26 < n && n <= 39)
      System.out.println("your card is hearts");
    if (39 < n && n <= 52)
      System.out.println("your card is spades");
      */
  }
}