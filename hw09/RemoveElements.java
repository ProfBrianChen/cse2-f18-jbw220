
import java.util.Random;
import java.util.Arrays;
import java.util.Scanner;
public class RemoveElements {
    public static int [] randomInput(int [] input){
        for (int i = 0; i < input.length; i++){
            Random rand = new Random();
            input [i] = rand.nextInt(10) + 0;

        }
        System.out.println(Arrays.toString(input));
        return input;
    }
    public static int [] deletion(int [] selectArray, int [] secondArray, int pos){

        for (int i = 0; i <secondArray.length; i++){
            if (i <= pos){
                secondArray [i] = selectArray[i];
            }
            else if (i > pos){
                secondArray [i] = selectArray[i+1];
            }
        }
        System.out.println(Arrays.toString(secondArray));
        return secondArray;

    }
    public static int [] target(int [] selectArray, int target){
      int j = 0;
      for (int i = 0; i < selectArray.length; i++){
        if (selectArray[i] == target){
          j++;
        }
        int [] targetArray = new int [10-j];
        for (int k = 0; k < targetArray.length; k++){
          if (selectArray[k] != target){
           targetArray[k] = selectArray[k];
          }
        }
        
}
System.out.println("The number of times the number showed up was: "+ j);
      return targetArray;
    }
    public static void main (String [] args){
        int a = 10;
        int [] selectArray = new int [a];
        int [] secondArray = new int [a-1];
   

        selectArray = randomInput(selectArray);
        Scanner scan = new Scanner(System.in);
        System.out.println("Please enter the pos value:");
        int pos = scan.nextInt();
        while (pos >= selectArray.length || pos < 0){
            System.out.println("Error! Pos is out of bounds, choose another.");
            pos = scan.nextInt();
        }
        secondArray = deletion(selectArray, secondArray, pos);

        System.out.println("Please enter the target you want to remove.");
        int target = scan.nextInt();
        while (target < 0 || target > 9){
            System.out.println("Error! Target is out of bounds, choose another.");
            target = scan.nextInt();
        }
      target(selectArray, target);



    }
}
