//Jamie Wisnia
// HW 9
//Program 1
import java.util.Scanner;
import java.util.Arrays;
import java.util.Random;
public class Linear {

    public static void shuffleArray(int [] arrGrade){
        int n = arrGrade.length;
        Random rand = new Random();
        rand.nextInt();
        for (int i = 0; i < n; i++){
            int change = i + rand.nextInt(n - i);
            swap(arrGrade, i, change);
        }
    }
    public static void swap(int [] a, int i, int change) {
        int helper = a[i];
        a[i] = a[change];
        a[change] = helper;
    }
    public static int binarySearch2(int[] list, int choice) {
        int low = 0;
        int high = list.length - 1;
        while (high >= low) {
            int mid = (low + high) / 2;
            if (choice < list[mid]) {
                high = mid - 1;
            } else if (choice == list[mid]) {
                return mid;
            } else {
                low = mid + 1;
            }
           // System.out.println("The grade was found.");
        }
        //System.out.println("The grade is not found.");
        return -1;
    }

    public static void main (String [] args) {
        int[] arrGrade = new int[15];
        int[] temp = new int[15];

        Scanner scan = new Scanner(System.in);
        int j = 0;
        // Outside for statement
        for (int i = 0; i < 15; i++) {
            // If the user tries to put in anything other than an integer.
            System.out.println("Please enter an int, that follows all the conditions:");
            boolean correctInt = scan.hasNextInt();
            while (correctInt == false) {
                System.out.println("Error! Please input an int.");
                scan.next();
                correctInt = scan.hasNextInt();

            }
            // If the user tries to put in anything outside of the range of numbers.
            int input = scan.nextInt();
            while (input > 100 || input < 0) {
                System.out.println("Error! Please input an int in the range.");
                input = scan.nextInt();
            }
            // If the user tries to put in a number smaller than previously.
            if (i == 0) {
                arrGrade[i] = input;
                temp[i] = input;
            } else {
                temp[i] = input;
                while (temp[j] <= temp[i - 1]) {
                    System.out.println("Error! Please input an ascending int.");
                    input = scan.nextInt();
                    temp[i] = input;
                }
                // Putting the temporary array equal to the final array.
                arrGrade[i] = temp[i];

            }
            j++;


        }
        //This prints out the array the user inputed
        System.out.println(Arrays.toString(arrGrade));
        System.out.println("Enter a grade to search for:");
        int choice = scan.nextInt();
        //System.out.println("The choice is " + choice);


        int indKey = binarySearch2(arrGrade, choice);
        if (indKey == -1) {
            //System.out.println("The grade was not found.");
            System.out.print(choice);
            System.out.print(" was not found in the ");
            System.out.println("15 iterations.");
                }
        else {
            System.out.print(choice);
            System.out.print(" was found in the ");
            System.out.println(indKey + " iterations.");


        }
        //This prints out the randomly shuffled array
        shuffleArray(arrGrade);
        int [] scramArr = new int [15];
        for (int w = 0; w < 15; w++){
            scramArr[w] = arrGrade[w];
        }
            System.out.println("This is the scrambled array:");
            System.out.println(Arrays.toString(scramArr));

            //Look for another grade after it is scrambled
            System.out.println("Enter a grade to search for:");
            int choiceScram = scan.nextInt();
            int indScram = binarySearch2(scramArr, choiceScram);
        if (indScram == -1) {
            //System.out.println("The grade was not found.");
            System.out.print(choiceScram);
            System.out.print(" was not found in the ");
            System.out.println("15 iterations.");
        }
        else {
            System.out.print(choiceScram);
            System.out.print(" was found in the ");
            System.out.println(indScram + " iterations.");


        }

        }
    }

