//Jamie Wisnia CSE 002 
import java.util.Scanner;

public class Methods {
  //Main method 
  //Scanner in the global area (For some reason, having this outside my main method (globally) helps me)
    static Scanner reader = new Scanner(System.in);
//Main method 
    public static void main(String [] args){
        System.out.println("Please enter your prefered output:");
        printMenu();
    }

    //Implement a sampleText() method that prompts the user to enter a string
    //Of their choosing. Store the text in a string. Output the string.

    //Implement a printMenu() method which outputs a menu of user options by
    //analyzing/editing the string, and returns the users entered menu option.
    //Each option is represented by a single character:
    //c - Number of non-whitespace characters
    //w - Number of words
    // f - Find text
    // r - Replace all !'s
    // s - Shorten spaces
    // q - Quit

  //Allows the user to enter the sample text. 
    public static String sampleText () {
        System.out.println("Enter a sample text: ");
        String text = reader.nextLine();
        System.out.println("You entered: " + text);
        return text;
    }
//This is the print menu method
    public static void printMenu () {
        boolean running = true;
        String text = sampleText();
      //Prints out all of the options
        System.out.println("MENU ");
        System.out.println("c - Number of non-whitespace characters");
        System.out.println("w - Number of words");
        System.out.println("f - Find text");
        System.out.println("r - Replace all !'s");
        System.out.println("s - Shorten spaces");
        System.out.println("q - Quit \n");
//While statement to use the print menu 
        while(running){
            Scanner reader = new Scanner(System.in);
            System.out.println("Choose an option: ");
//What the user inputs 
            String choice =reader.nextLine();
//The number of non-whitespace characters
            if(choice.equals("c")){
                int numNonWSChar = getNumOfNonWSCharacters(text);
                System.out.println("Number of non-whitespace characters: " + numNonWSChar);
            }
          //Number of words 
            else if(choice.equals("w")){
                int numOfWords = getNumOfWords(text);
                System.out.println("Number of words: " + numOfWords);
            }
          // Find the text 
            else if(choice.equals("f")){
                System.out.println("Enter a word or phrase to be found: ");
                String keyWord = reader.nextLine();
              //User inputs the word they want to find 
                int numOfKW = findText(keyWord, text);
                System.out.println("\"" + keyWord + "\"" + " instances: " + numOfKW);
            }
          //Exclamation marks 
            else if(choice.equals("r")){
                text = replaceExclamation(text);
                System.out.println("Edited text: " + text);
            }
          //Shorten the extended spaces 
            else if(choice.equals("s")){
                text = shortenSpace(text);
                System.out.println("Edited text: " + text);
            }
          //Quit the program 
            else if(choice.equals("q")){
                running = false;
                System.exit(0);
            }
            else {
                System.out.println("Please select a valid choice. ");
            }
        }
    }
// Method to get the number of non whitespaces 
    public static int getNumOfNonWSCharacters(String text) {
        int numNonWSChar = 0;
        int end = text.length() - 1;
        for (int i = 0; i < text.length(); i++) {
            if (text.charAt(i) != ' ') {
                numNonWSChar++;
            }
        }
        return numNonWSChar;
    }
//How to find the number of words 
    public static int getNumOfWords(String text) {
        int numWords = 0;
        boolean ifWord = false;
        int end = text.length() - 1;
        for (int i = 0; i <= end; i++) {
            if (text.charAt(i) == '\'') {
                numWords--;
            }
            if (Character.isLetter(text.charAt(i)) && i != end) {
                ifWord = true;
            } else if (!Character.isLetter(text.charAt(i)) && ifWord) {
                numWords++;
                ifWord = false;
            } else if (Character.isLetter(text.charAt(i)) && i == end) {
                numWords++;
            }
        }
        return numWords;
    }
//How to find the specific word in the text 
    public static int findText(String keyWord, String text) {
        int found = 0;
        for (int i = 0; i < text.length(); i++) {
            if(text.substring(i).contains(keyWord)) {
                found++;
                i+=text.substring(i).indexOf(keyWord);
            }
        }
        return found;
    }
//How to find the exclamation point! 
    public static String replaceExclamation(String text) {
        while(text.contains("!")) {
            int spot = text.indexOf("!");
            text = text.substring(0,spot) + "." + text.substring(spot+1);
        }
        return text;
    }
// How to shorten the spaces 
    public static String shortenSpace(String text) {
        while(text.contains("  ")) {
            int spot = text.indexOf("  ");
            text = text.substring(0,spot) + text.substring(spot+1);
        }
        return text;
    }
}