//Jamie Wisnia CSE 002 Lab 007 
import java.util.Random;
import java.util.Scanner;
//This code uses passing as value without method overloading
public class Methods {
        public static void main(String[] args) {
// Main method, constant random for the noun of the sentence 
            Random rand = new Random();
            int rand2 = rand.nextInt(10) + 1;

          //While statement to run the loop of the code 
          boolean run = true;
            while(run) {
                int rand1 = rand.nextInt(10) + 1;
                int rand3 = rand.nextInt(10) + 1;
                int rand4 = rand.nextInt(10) + 1;
                int rand5 = rand.nextInt(10) + 1;
                System.out.print("The");
                System.out.print(" ");
                Adjectives(rand1);
                System.out.print(" ");
                NonPrimary(rand2);
                System.out.print(" ");
                PastTense(rand3);
                System.out.print(" ");
                System.out.print("the");
                System.out.print(" ");
                Adjectives(rand4);
                System.out.print(" ");
                NonPrimaryObject(rand5);
                System.out.print(".");
                run = UserInput();
            }
        }


        public static void Adjectives(int rand) {
//Switch statment for the Adjectives 
            switch (rand) {
                case 1:
                    System.out.print("pretty ");
                    break;
                case 2:
                    System.out.print("aggressive ");
                    break;
                case 3:
                    System.out.print("agreeable ");
                    break;
                case 4:
                    System.out.print("ambitious ");
                    break;
                case 5:
                    System.out.print("calm ");
                    break;
                case 6:
                    System.out.print("delightful ");
                    break;
                case 7:
                    System.out.print("eager ");
                    break;
                case 8:
                    System.out.print("ugly");
                    break;
                case 9:
                    System.out.print("supportive");
                    break;
                case 10:
                    System.out.print("loving");
                    break;

            }

        }
//Switch statement for the past tense verbs 
        public static void PastTense(int rand) {

            switch (rand) {
                case 1:
                    System.out.print("ran");
                    break;
                case 2:
                    System.out.print("honked");
                    break;
                case 3:
                    System.out.print("admired");
                    break;
                case 4:
                    System.out.print("lied");
                    break;
                case 5:
                    System.out.print("said");
                    break;
                case 6:
                    System.out.print("accepted");
                    break;
                case 7:
                    System.out.print("rejected");
                    break;
                case 8:
                    System.out.print("danced");
                    break;
                case 9:
                    System.out.print("alerted");
                    break;
                case 10:
                    System.out.print("amused");
                    break;

            }
        }
//Switch statment for the non primary nouns 
        public static int NonPrimary(int rand) {

            switch (rand) {
                case 1:
                    System.out.print("dog");
                    break;
                case 2:
                    System.out.print("horse");
                    break;
                case 3:
                    System.out.print("pig");
                    break;
                case 4:
                    System.out.print("cup");
                    break;
                case 5:
                    System.out.print("watterbottle");
                    break;
                case 6:
                    System.out.print("Thomas");
                    break;
                case 7:
                    System.out.print("Julia");
                    break;
                case 8:
                    System.out.print("Barbie");
                    break;
                case 9:
                    System.out.print("Molly");
                    break;
                case 10:
                    System.out.print("Sawyer");
                    break;

            }
            return rand;
        }
//switch statement for the non primary object noun 
        public static void NonPrimaryObject(int rand) {

            switch (rand) {
                case 1:
                    System.out.print("lamp");
                    break;
                case 2:
                    System.out.print("horn");
                    break;
                case 3:
                    System.out.print("chair");
                    break;
                case 4:
                    System.out.print("table");
                    break;
                case 5:
                    System.out.print("leash");
                    break;
                case 6:
                    System.out.print("phone");
                    break;
                case 7:
                    System.out.print("cord");
                    break;
                case 8:
                    System.out.print("tv");
                    break;
                case 9:
                    System.out.print("couch");
                    break;
                case 10:
                    System.out.print("remote");
                    break;

            }
        }
//If the user wants to see another sentence 
        public static boolean UserInput() {
            System.out.println(" ");
            System.out.println("Would you like to see another sentence?");
            System.out.println("Choose yes or no.");

            Scanner reader = new Scanner(System.in);
            String choice = reader.nextLine();

            if (choice.equals("yes")) {
                return true;

            } else if (choice.equals("no")) {
                return false;
            }

            return false;

        }




    }


