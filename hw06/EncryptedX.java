//Final HW #6 Solution: Jamie Wisnia 10/22/18: CSE 002
import java.util.Scanner;

public class EncryptedX {
    public static void main(String[] args) {
        //Initializing my variables as integers
        int i, j;
        //Importing my scanner to ask for user input
        Scanner scan = new Scanner(System.in);
        System.out.println("Please enter an integer between 1 and 100. ");
        //True is for integer, false is for not
        boolean check = scan.hasNextInt();

        // I set h as a random number in case it was an integer and between 1-10
        int h = -999999;
        // If the user wrote anything other than an integer
        while (check == false) {
            System.out.println("Please write an integer value.");
            scan.next();

            if (scan.hasNextInt()) {
                check = scan.hasNextInt();

                h = scan.nextInt();
                //If the user wrote an number outside of our range
                while (h < 1 || h > 100) {
                    System.out.println("Error: not between 1 and 100. Please try again.");
                    h = scan.nextInt();
                }
            }
        }
        //If the user wrote the proper number
        if (h == -999999) {
            h = scan.nextInt();
            while (h < 1 || h > 100){
                System.out.println("Error: not between 1 and 100. Please try again.");
                h = scan.nextInt();
            }
        }
        //This will decrease each asterick from h to whenever until i is done.
        //Outer loop: This increments how many columns I will have
        for (i = 0; i <= h; i++) {
            //Nested loop: This lets me know how many spaces I want per row.
            for (j = 0; j <= h; j++) {
                //If statement: This decides whether a space or * will be printed.
                //This is the pattern for when to print the spaces
                if (j == i || j == h - i) {
                    System.out.print(" ");
                    //Besides spaces, this prints the *
                } else {
                    System.out.print("*");
                }
            }
            //This goes to the next line
            System.out.println();
        }
    }
}
