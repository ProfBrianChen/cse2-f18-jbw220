//Lab 006 Pattern B Jamie Wisnia
import java.util.Scanner;

public class PatternB {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Please enter the height of the pyramid (an integer between 1-10). ");
        //True is for integer, false is for not
        boolean check = scan.hasNextInt();

        // I set h as a random number in case it was an integer and between 1-10
        int h = -9999;
        // If the user wrote anything other than an integer
        while (check == false) {
            System.out.println("Please write an integer value.");
            scan.next();

            if (scan.hasNextInt()) {
                check = scan.hasNextInt();

                h = scan.nextInt();
                //If the user wrote an number outside of our range
                while (h < 1 || h > 10) {
                    System.out.println("Error: not between 1 and 10. Please try again.");
                    h = scan.nextInt();
                }
            }
        }
        //If the user wrote the proper number
        if (h == -9999) {
            h = scan.nextInt();
            while (h < 1 || h > 10){
                System.out.println("Error: not between 1 and 10. Please try again.");
                h = scan.nextInt();
            }
        }
        for (int i = h; i >=1; i--) {
            //This increments horizontally, how many things I should have
            for (int j = 1; j <= i; j++) {
                System.out.print(j + " ");
            }
            System.out.println();
        }


    }
}

