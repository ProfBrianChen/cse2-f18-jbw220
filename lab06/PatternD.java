//Lab 006 PatternD Jamie Wisnia
import java.util.Scanner;

public class PatternD {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Please enter the height of the pyramid (an integer between 1-10). ");
        //True is for integer, false is for not
        boolean check = scan.hasNextInt();

        // I set h as a random number in case it was an integer and between 1-10
        int h = -9999;
        // If the user wrote anything other than an integer
        while (check == false) {
            System.out.println("Please write an integer value.");
            scan.next();

            if (scan.hasNextInt()) {
                check = scan.hasNextInt();

                h = scan.nextInt();
                //If the user wrote an number outside of our range
                while (h < 1 || h > 10) {
                    System.out.println("Error: not between 1 and 10. Try again.");
                    h = scan.nextInt();
                }
            }
        }
        //If the user wrote the proper number
        if (h == -9999) {
            h = scan.nextInt();
            //If the user wrote an integer, but it was outside of range.
            while (h <1 || h > 10) {
                System.out.println("Error: not between 1 and 10. Try again. ");
                h = scan.nextInt();
            }
        }
        //This increments how many lines I have
        for (int i = h; i >= 0; i--) {
            //This increments horizontally, how many things I should have
            for (int j = i; j >= 1; j--) {
                System.out.print(j + " ");
            }
            System.out.println();
        }

    }
}




