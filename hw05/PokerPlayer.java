//HW #5 Poker Player Generator Jamie Wisnia October 9th 2018
import java.util.Scanner;

public class PokerPlayer {
    public static void main(String[] args) {
        Scanner myScanner = new Scanner(System.in);
        int onePairCounter = 0;
        int twoPairCounter = 0;
        int threeKind = 0;
        int fourKind = 0;
        double pOne = 0;
        double pTwo = 0;
        double pThree = 0;
        double pFour = 0;

        System.out.println("Input the number of hands to generate.");
        //...we'll trust the user
        int numHands = myScanner.nextInt();
        for (int i = 1; i <= numHands; i++) {
            // Generate 2 distinct cards
            int card1 = (int) (Math.random() * (52)) + 1;

            int card2 = (int) (Math.random() * (52)) + 1;

            int card3 = (int) (Math.random() * (52)) + 1;

            int card4 = (int) (Math.random() * (52)) + 1;

            int card5 = (int) (Math.random() * (52)) + 1;

            while (card2 == card1) {
                //myScanner.nextLine();
                card2 = (int) (Math.random() * (52)) + 1;
            }
            while (card3 == card2 || card3 == card1) {
                //myScanner.nextLine();
                card3 = (int) (Math.random() * (52)) + 1;
            }
            while (card4 == card3 || card4 == card2 || card4 == card1) {
                //myScanner.nextLine();
                card4 = (int) (Math.random() * (52)) + 1;
            }
            while (card5 == card4 || card5 == card3 || card5 == card2 || card5 == card1) {
                //myScanner.nextLine();
                card5 = (int) (Math.random() * (52)) + 1;
            }
            System.out.println("card 1 = " + card1);
            System.out.println("card 2 = " + card2);
            System.out.println("card 3 = " + card3);
            System.out.println("card 4 = " + card4);
            System.out.println("card 5 = " + card5);

            card1 = card1 % 13;
            System.out.println("card 1 = " + card1);
            card2 = card2 % 13;
            System.out.println("card 2 = " + card2);
            card3 = card3 % 13;
            System.out.println("card 3 = " + card3);
            card4 = card4 % 13;
            System.out.println("card 4 = " + card4);
            card5 = card5 % 13;
            System.out.println("card 5 = " + card5);

            //Single Pair Check
            if (card1 == card2 || card1 == card3 || card1 == card4 || card1 == card5) {
                //myScanner.nextLine();
                onePairCounter++;


                //Two Pair Check
                if ((card1 == card2) && (card3 == card4 || card3 == card5 || card4 == card5)){
                    twoPairCounter++;
                }
                else if ((card1 == card3) && (card2 == card4 || card2 == card5 || card4 == card5)){
                    twoPairCounter++;
                }
                else if ((card1 == card4) && (card3 == card5 || card3 == card2 || card5 == card2)){
                    twoPairCounter++;
                }
                else if ((card1 == card5) && (card4 == card2 || card4 == card3 || card2 == card3)){
                    twoPairCounter++;
                }

                //Three of a Kind Check
                if (card1 == card2 && card1 == card3 || card1 == card2 && card1 == card4 || card1 == card2 && card1 == card5) {
                    threeKind++;

                    // Four of a kind Check
                    if (card1 == card2 && card1 == card3 && card1 == card4 || card1 == card2 && card1 == card3 && card1 == card5) {
                        fourKind++;
                    }
                //Three of a Kind Check
                } else if (card1 == card3 && card1 == card4 || card1 == card3 && card1 == card5) {
                    threeKind++;


                    //Four of a kind Check
                    if (card1 == card3 && card1 == card4 && card1 == card5) {
                        fourKind++;
                    }
                // Three of a Kind Check
                } else if (card1 == card4 && card1 == card5) {
                    threeKind++;
                }
            }
            //Single Pair Check
            else if (card2 == card3 || card2 == card4 || card2 == card5) {
                //myScanner.nextLine();
                onePairCounter++;

                // Double Pair Check
                 if ((card2 == card3) && (card1 == card4 || card1 == card5 || card4 == card5)){
                    twoPairCounter++;
                }
                else if((card2 == card4) && (card1 == card3 || card1 == card5 || card3 == card5)){
                    twoPairCounter++;
                }
                else if((card2 == card5) && (card1 == card3 || card1 == card4 || card3 == card4)){
                    twoPairCounter++;
                }


                //Three of a Kind Check
                if (card2 == card3 && card2 == card4 || card2 == card3 && card2 == card5) {
                    threeKind++;

                    //Four Kind Check
                    if (card2 == card3 && card2 == card4 && card2 == card5) {
                        fourKind++;
                    }
                //Three of a Kind Check
                else if (card2 == card4 && card2 == card5) {
                        threeKind++;
                    }
                }
            //Single Pair Check
            else if (card3 == card4 || card3 == card5) {
                onePairCounter++;

               // Double Pair Check
                if((card3 == card4) && (card2 == card1 || card2 == card5 || card5 == card1)){
                        twoPairCounter++;
                    }
                    else if((card3 == card5) && (card2== card1 || card2 == card4 || card1 == card4)){
                        twoPairCounter++;
                    }

                    // Three of a Kind Check
                    if (card3 == card4 && card3 == card5) {
                        threeKind++;
                    }
                }
            //Single Pair Check
            else if (card4 == card5) {
                onePairCounter++;
                // Double Pair Check
                    if ((card4 == card5) && (card1 == card2 || card1 == card3 || card2 == card3 )){
                        twoPairCounter++;
                    }
                }
            }
            pOne = ((double) onePairCounter / (double) numHands);
            /*System.out.println("The number of single pairs is: " + onePairCounter);
            System.out.println("The Probability of a single pair is: " + String.format("%.3f", pOne));*/
            pThree = ((double) threeKind / (double) numHands);
            /*System.out.println("The number of three of a kind is: " + threeKind);
            System.out.println("The probability of a three of a kind is: " + String.format("%.3f", pThree));*/
            pFour = ((double) fourKind / (double) numHands);
            /*System.out.println("the number of four of a kind is: " + fourKind);
            System.out.println("the probability of a four of a kind is: " + String.format( "%.3f", pFour));*/
            pTwo = ((double) twoPairCounter / (double) numHands);
            /*System.out.println("The number of double pairs is: " + twoPairCounter);
            System.out.println("The probability of double pairs is: " + String.format( "%.3f", pTwo));*/


        }
        System.out.println("The number of pairs is: " + onePairCounter);
        System.out.println("The Probability of a single pair is: " + String.format("%.3f", pOne));

        System.out.println("The number of three of a kind is: " + threeKind);
        System.out.println("The probability of a three of a kind is: " + String.format("%.3f", pThree));

        System.out.println("the number of four of a kind is: " + fourKind);
        System.out.println("the probability of a four of a kind is: " + String.format( "%.3f", pFour));

        System.out.println("the number of four of a kind is: " + fourKind);
        System.out.println("the probability of a four of a kind is: " + String.format( "%.3f", pFour));

        System.out.println("The number of double pairs is: " + twoPairCounter);
        System.out.println("The probability of double pairs is: " + String.format( "%.3f", pTwo));
    }
}
