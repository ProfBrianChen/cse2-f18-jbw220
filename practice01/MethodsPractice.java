import java.util.Scanner;

public class MethodsPractice {
	
	public static void main(String[] args){
		
		// Think of the main method as a summary of what your code should do.
		// Problem: Provide the user with the area of a plot of land, given
		// 			its dimensions.  Then determine whether or not the yard is
		//			square, as well as if it matches a predetermined area.
		// 			Finally, print a meaningful statement to terminating the program.
		
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Input the yard width.");
		int yardWidth = myScanner.nextInt();
		System.out.println("Input the yard height.");
		int yardHeight = myScanner.nextInt();
		// zyBooks discusses how to pass a Scanner as an input - can you modify
		// this demonstration to include a method that handles the four previous lines?
		
		// Determine the area of the yard
		int area = yardWidth*yardHeight;
		//int area = rectangleArea(yardWidth, yardHeight);
		System.out.println("The area of your plot of land is " +
				area + ".");
		
		// Determine whether the yard is square or not and print 
		// that info
		boolean square;
		if (yardWidth == yardHeight) {
			square = true;
		}
		else {
			square = false;
		}
		
		if (square) {
			System.out.println("The yard is square.");
		}
		else {
			System.out.println("The yard is not square.");
		}
		//boolean square = findEqual(yardWidth, yardHeight);
		//Not all methods have output
		//printPoly(square);
		
		// Determine if the area of the user's yard equals that of a 
		// predetermined area
		int area2 = 100;
		if (area == area2) {
			System.out.println("The yards have equal area.");
		}
		else {
			System.out.println("The yards do not have equal area.");
		}
		//boolean sameArea = findEqual(area2, area);
		//printArea(sameArea);
		
		// Print a statement to signify the end of the program
		//System.out.println("This concludes our demo introducing methods.");
		//And some methods don't even take input
		//printInfo();
				
	} // End of main method
	
	// Begin all auxilliary functions here

	public static int rectangleArea( int width, int height ){
		return (width*height);
	}

	/*public static double findAvg( int a, int b ){
		double avg = (a+b)/2;   
          	if(avg > 2)
			return true;     //WRONG! compiler error
	}*/
	
	public static boolean findEqual (int a, int b){
		if(a == b) {
			return true;
		}
		else{
			return false;
		}
		// int sum = a + b; //Wrong! This is unreachable  code - where else have we seen this?
	}
	
	public static void printPoly(boolean printYes) {
		if(printYes) {
			System.out.println("The yard is square.");
		}
		else {
			System.out.println("The yard is not square.");
		}
		return;
	}
	// Can you condense printPoly with printArea to one method
	public static void printArea(boolean printYes) {
		if(printYes) {
			System.out.println("The yards have equal area.");
		}
		else {
			System.out.println("The yards do not have equal area.");
		}
	}
	
	public static void printInfo(  ){
		System.out.println("This concludes our demo introducing methods.");
		return;
	}



}