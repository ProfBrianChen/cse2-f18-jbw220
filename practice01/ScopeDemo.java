import java.util.Scanner;

public class ScopeDemo{
	public static void main(String[] args){
		Scanner myPauseScanner = new Scanner(System.in);
		
		// Let's return to our scope example
		int val2 = 4, val3 = 5;
		//String myString;// = "Error: String not initialized."
		//if( val2 < val3 ){
			//String myString = "whoa!";
		//}
		//System.out.println( val3 + myString );
		
		// Variable shadowing
		double rentRate = 25.0;
		if( true ){
			rentRate = 35.0;
		}
		System.out.println("Rent Rate:" + rentRate);



		
	}
	
}