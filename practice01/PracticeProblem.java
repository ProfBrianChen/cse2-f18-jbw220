import java.util.Scanner;

public class PracticeProblem{
	public static void main(String[] args){
		Scanner myPauseScanner = new Scanner(System.in);
		
		// Examining post increment and the augmented operator
		int i, j, k;
		i = j = k = 2;
		if (i++ < ++j){ // 2 < 3 
    	switch (j++){ // j = 3 
        case (1):
            System.out.printf("case 1 ");
            k %= 2;
            break;   
        case (2):
            System.out.printf("case 2 ");
            k /= 3;
            break;   
        case (3):
            System.out.printf("case 3 ");
            k -= 4;
            break;  // What will happen if we remove the break here? 
        default:
            System.out.printf("default ");
            k *= 5;
            break;  // What happens if we remove all breaks?
    	} // j = 4 
    	System.out.printf("i+j+k = %2.1f", (double)(i - j + ++k));
		}
		else {
    	i++;   
    	System.out.printf("else ");
    	System.out.printf("i+j+k = %2.1f", (double)(i - j + k++));
		}
		System.out.printf("\n");
		
		myPauseScanner.nextLine();
	
		// Let's examine associativity - What is printed to STDOUT?
		System.out.println(5 * 2 == 45 != 23 > 63 == true);
		
		myPauseScanner.nextLine();
		
		//System.out.println(12 * 4 + (9 > 4) / 2);
		
		// myPauseScanner.nextLine();
		
		System.out.println(10 > 9 || (3 + 1) / (1 - 1) > 0);
		
		// Compare the previous statement with this:
		// System.out.println(10 > 9 | (3 + 1) / (1 - 1) > 0);
		
		myPauseScanner.nextLine();
		
		// Let's examine addition/appendation with Strings
		int month = 4; String day = "24"; double val1 = .98;
		
		System.out.println(5+month+day+2018);
		
		myPauseScanner.nextLine();
		
		// Let's examine precedence
		// int m = 3;
		// System.out.println((int) 2.3 / (double) 4.4 > m++ && 2 > 3 % 2 || 4 >= 5 && true);
		
		
		myPauseScanner.nextLine();
		// Let's reexamine scope
		String myString = " Error";
		int val2 = 7, val3 = 5;
		if( val2 < val3 ){
			myString = " whoa!";
		}
		System.out.println( val3 + myString );

		
	}
	
}