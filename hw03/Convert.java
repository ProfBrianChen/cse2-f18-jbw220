//////////////
/// CSE 002 Convert HW #3 Jamie Wisnia
///
import java.util.Scanner;
public class Convert{
 //main method required for every Java program
  public static void main(String[] args){
    //Declare an instance of the scanner object and call the Scanner constructor
    Scanner myScanner = new Scanner( System.in);
    //Prompt the user for the number of acres affected
    System.out.print("Enter the affected area in acres in the form xxxxx.xx: ");
    //Accept user input by using this statement
    double acreNumber = myScanner.nextDouble();
    //Prompt the user for the number of inches of rainfall
    System.out.print("Enter the rainfall in the affected area in the form xx: ");
    //Accept user input by using this statement
    double inchesNumber = myScanner.nextDouble();
    // We want to convert to Acre-Inches
    double acreInch = ((acreNumber) * (inchesNumber));
    // Convert Acre-Inches into Cubic Miles
    double cubicMile = ((acreInch)/(40550399.587131));
    // Print the answer
      System.out.printf("The amount in Cubic Miles is: %.8f\n",cubicMile);
  }
  
}