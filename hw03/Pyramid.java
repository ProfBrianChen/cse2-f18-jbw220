//////////////
/// CSE 002 HW #3 Jamie Wisnia
///
import java.util.Scanner;
public class Pyramid{
  // Main method required for every Java program
  public static void main(String[]args){
    //Declare an instance of the scanner object and call the Scanner constructor
    Scanner myScanner = new Scanner( System.in);
    //Prompt the user for length and width of the square base
    System.out.print("The square side of the pyramid is (input length): ");
    //Accept user input by using this statement
    double lengthWidth = myScanner.nextDouble();
    //Prompt the user for the number of inches of rainfall
    System.out.print("The height of the pyramid is (input height): ");
    //Accept user input by using this statement
    double heightNum = myScanner.nextDouble();
    //Calculate the volume of the pyramid: NOTE V = (1/3)l*w*h
    // First calculate l*w*h
    double sideValues = ((lengthWidth)*(lengthWidth)*(heightNum));
    // Now calculate overall volume
    double volPyramid = ((sideValues)/3);
    // Print the value of the pyramid
    System.out.printf("The Volume of the Pyramid is:%.0f\n", +volPyramid);
  }
}