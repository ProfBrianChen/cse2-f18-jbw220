//////////////
/// CSE 002 Arithmetic HW Jamie Wisnia 
///
public class Arithmetic{
   
 public static void main(String args[]){
   
  
//the tax rate
double paSalesTax = 0.06;
   
//Number of pairs of pants
int numPants = 3;
//Cost per pair of pants
double pantPrice = 34.98;
//Total pants Cost
double pantCost = ((pantPrice)*(numPants));   
//Tax on the pants
double pantTax = ((pantCost)*(paSalesTax));
//Total price of pants with the tax
double pantCostTax = ((pantCost) + (pantTax));   
//The total price of the pants 
System.out.println("Pant Cost: $" + pantCost);
 //The total cost of the pant tax
System.out.printf("Tax on pants: $%.2f\n", pantTax);   
 // The overall price with tax of the pants 
System.out.printf("Pant price with tax: $%.2f\n",  pantCostTax);    

//Number of sweatshirts
int numShirts = 2;
//Cost per shirt
double shirtPrice = 24.99;
//Total shirts Cost
double shirtCost = ((shirtPrice)*(numShirts));   
//Tax on the shirts
double shirtTax = ((shirtCost)*(paSalesTax));
//Total price of shirts with the tax
double shirtCostTax = ((shirtCost) + (shirtTax));  
//The total price of the shirts 
System.out.println("Shirt Cost: $" + shirtCost);
 //The total cost of the shirt tax
System.out.printf("Tax on shirts: $%.2f\n", shirtTax);   
 // The overall price with tax of the shirts 
System.out.printf("Shirt price with tax: $%.2f\n", shirtCostTax);    
   
   

//Number of belts
int numBelts = 1;
//cost per belt
double beltPrice = 33.99;
//Total belt Cost
double beltCost = ((beltPrice)*(numBelts));   
//Tax on the belts
double beltTax = ((beltCost)*(paSalesTax));
//Total price of belts with the tax
double beltCostTax = ((beltCost) + (beltTax));   
//The total price of the belts 
System.out.println("Belt Cost: $" + beltCost);
 //The total cost of the belt tax
System.out.printf("Tax on belts: $%.2f\n", beltTax);   
 // The overall price with tax of the pants 
System.out.printf("Belt price with tax: $%.2f\n", beltCostTax);    
   
//The total cost of purchases before tax
double clothesPrice = ((pantCost) + (shirtCost) + (beltCost));
// The total price of the clothes before tax
System.out.println("The total price of all clothing before tax: $" + clothesPrice);      
//The total Tax
 double clothesTax = ((pantTax) + (shirtTax) +(beltTax));
 // The total price of the tax statment 
 System.out.printf("The total cost of tax is: $%.2f\n", clothesTax);    
 // The total cost of purchases with tax
 double clothesTotal = ((clothesPrice) + (clothesTax));
 // The overall cost 
 System.out.printf("The total price of all clothing after tax: $%.2f\n", clothesTotal);     
   
   
   
   
   


 } 
}