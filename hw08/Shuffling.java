/*Basically, you will be given a deck of 52 cards,
represented by the array cards of Strings. You will have to print
out all the cards in the deck, shuffle the whole deck of cards, then print
 out the cards in the deck, all shuffled, then getting a hand of cards and
print them out.
 */

import java.util.Random;


import java.util.Scanner;
public class Shuffling{
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        //suits club, heart, spade or diamond
        String[] suitNames={"C","H","S","D"};
        String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"};
        String[] cards = new String[52];
        //String[] hand = new String[5];
        int numCards = 5;
        int again = 1;
        int index = 51;
        for (int i=0; i<52; i++){
            cards[i]=rankNames[i%13]+suitNames[i/13];
            //System.out.print(cards[i]+" ");
        }
        System.out.println();
        System.out.println("This is the regular version");
        printArray(cards);
        System.out.println();

        System.out.println("This is the shuffled version");
        shuffle(cards);
//        printArray(cards);
        while(again == 1){
            getHand(cards,index,numCards);
            //printArray(hand);
            //index = index - numCards;
            System.out.println("Enter a 1 if you want another hand drawn");
            again = scan.nextInt();
        }
    }
    public static void printArray(String[] cards){

        for(int i = 0; i < cards.length; i++){
            System.out.print(cards[i] + ", ");
        }

    }
    public static void shuffle (String[] cards){
        Random rand = new Random();
        String[] shuffled = new String[52];
        for(int i = 0; i < 52; i++){
            int j = rand.nextInt(52);
            if (!(cards[j].equals(""))){
                shuffled[i] = cards[j];
                cards[j] = "";
            }
            else{
                i-=1;
            }
        }
        printArray(shuffled);


    }
    public static void getHand(String[] cards, int index, int numCards){
        Random rand = new Random();
        String[] hand = new String[index];
        for(int i =0; i < index; i++){
            int j = rand.nextInt(numCards);
            hand[i] = cards[j];
        }

        printArray(hand);

    }
}

