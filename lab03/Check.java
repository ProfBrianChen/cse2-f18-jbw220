//////////////
//// CSE 002 Lab 03 Check: Jamie Wisnia 
///
import java.util.Scanner;
public class Check{
                // main method required for every Java program
               public static void main(String[] args) {
// Declare an instance of the scanner object and call the Scanner constructor.  
                Scanner myScanner = new Scanner( System.in );
                 //Prompt the user for the original cost of the check
System.out.print("Enter the original cost of the check in the form xx.xx: ");
                 //Accept user input by using the statement
                 double checkCost = myScanner.nextDouble();
// Let the user enter the percent of tip
System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): ");
// Accept user input                  
double tipPercent = myScanner.nextDouble();
                 // we want to convert the fraction into a decimal place
tipPercent /= 100; 
// Ask user how many people went out to dinner
System.out.print("Enter the number of people who went out to dinner: ");  
// Accept user input
int numPeople = myScanner.nextInt();
// Print out the output
double totalCost;
double costPerPerson;
int dollars,   //whole dollar amount of cost 
      dimes, pennies; //for storing digits
          //to the right of the decimal point 
          //for the cost$ 
totalCost = checkCost * (1 + tipPercent);
costPerPerson = totalCost / numPeople;
//get the whole amount, dropping decimal fraction
dollars=(int)costPerPerson;
//get dimes amount, e.g., 
// (int)(6.73 * 10) % 10 -> 67 % 10 -> 7
//  where the % (mod) operator returns the remainder
//  after the division:   583%100 -> 83, 27%5 -> 2 
dimes=(int)(costPerPerson * 10) % 10;
pennies=(int)(costPerPerson * 100) % 10;
System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies);

                 



//end of main method
} 
                   //end of class
      } 

