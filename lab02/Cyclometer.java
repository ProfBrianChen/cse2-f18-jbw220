//Document your program. What does MPG do? Place your comments here! //
public class Cyclometer {
  // main method required for every Java program 
  public static void main(String[] args) {
    // our input data
    int secsTrip1=480; //Number of seconds after Trip1
    int secsTrip2=3220; //Number of seconds after Trip2
    int countsTrip1=1561; //Number of counts after Trip1
    int countsTrip2=9037; //Number of counts after Trip2
    
    //our intermediate variables and output data
    double wheelDiameter=27.0, // This is the value for the wheel diameter
    PI=3.14159, // This is the value for pi
    feetPerMile=5280, //This is how many feet are in a mile
    inchesPerFoot=12, //Another conversion
    secondsPerMinute=60; //Conversion
    double distanceTrip1, distanceTrip2, totalDistance; //distances for both trip 1 and 2
    
    //Print the numbers that have stored in the variables 
    System.out.println("Trip 1 took " + (secsTrip1/secondsPerMinute)+" minutes and had "+ countsTrip1+" counts.");
    System.out.println("Trip 2 took "+(secsTrip2/secondsPerMinute)+" minutes and had "+ countsTrip2+" counts.");
     
    //run the calculations; store the values. 
    distanceTrip1=countsTrip1*wheelDiameter*PI; //gives the distance in inches
    distanceTrip1/=inchesPerFoot*feetPerMile; //gives the distance in miles
    distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile; //gives the distance in miles 
    totalDistance=distanceTrip1+distanceTrip2; //total distance
    
    //print out the distances
    System.out.println("Trip 1 was "+distanceTrip1+" miles");
    System.out.println("Trip 2 was "+distanceTrip2+" miles");
    System.out.println("The total distance was "+totalDistance+" miles");


  } //end of main method
} //end of class