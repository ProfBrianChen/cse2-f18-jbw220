// Lab 05: User Input: Jamie Wisnia 10-5-18
////
///
import java.util.Scanner;
public class UserInput{
  public static void main (String[]args){
    Scanner scan = new Scanner(System.in);
    
    System.out.println("Please enter your course number.");
    boolean correctInt = scan.hasNextInt();
    while (correctInt == false ){
      System.out.println("Please write an integer value.");
      scan.next();
      correctInt = scan.hasNextInt();
    }
    int courseNum = scan.nextInt();
    
    System.out.println("Please enter your department name.");
    String departmentName = scan.next();
    
    System.out.println("Please enter the number of times you meet per week.");
     correctInt = scan.hasNextInt();
    while (correctInt == false ){
      System.out.println("Please write an integer value.");
      scan.next();
      correctInt = scan.hasNextInt();
    }
       int frequency = scan.nextInt();
    
    System.out.println("Please enter the time the class starts, in the form XXXX");
     correctInt = scan.hasNextInt();
    while (correctInt == false ){
      System.out.println("Please write an integer value.");
      scan.next();
      correctInt = scan.hasNextInt();
    }
      int time = scan.nextInt();
    
    System.out.println("Please enter the name of your Instructor.");
    String instructor = scan.next();
    
    System.out.println("Please enter the number of students in your class.");
     correctInt = scan.hasNextInt();
    while (correctInt == false ){
      System.out.println("Please write an integer value.");
      scan.next();
      correctInt = scan.hasNextInt();
    }
    int studentNum = scan.nextInt();
System.out.println("Your course number is:" +courseNum);
    System.out.println("Your department name is:" +departmentName);
    System.out.println("The number of times you meet per week is:" +frequency);
    System.out.println("Your time for class is:" +time);
    System.out.println("The name of your instructor is:" +instructor);
    System.out.println("The number of students in your class:" +studentNum);
  }
}